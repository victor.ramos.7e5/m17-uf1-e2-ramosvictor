﻿using GameTools;
using MyFirstProgram;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using matrixFinal;

namespace M17_UF1_E2_Ramos_Victor
{
    internal class GameStart : GameEngine
    {
        MatrixRepresentation main;
        MatrixRepresentation aux;
        PrintMatrix2 pintarm; //
        Random rnd;

        char[] letters;
        int letra;
        int charColumna;

        protected override void Start()
        {
            main = new MatrixRepresentation(25, 45); //25 , 45 
            aux = new MatrixRepresentation(main.TheMatrix.GetLength(0), main.TheMatrix.GetLength(1));

            main.CleanTheMatrix();
            aux.CleanTheMatrix();
        }

        protected override void Update()
        {
            letters = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'Ñ', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };
            pintarm = new PrintMatrix2();
            rnd = new Random();

            /
            CaiciondeLetras(6, rnd);

            
            for (int fila = 0; fila < main.TheMatrix.GetLength(0); fila++)
            {
                for (int columna = 0; columna < main.TheMatrix.GetLength(1); columna++)
                {

                   
                    if (main.TheMatrix[fila, columna] != '0')
                    {
                         if (fila != main.TheMatrix.GetLength(0) - 1)
                        {

                            aux.TheMatrix[fila + 1, columna] = main.TheMatrix[fila, columna];
                            if (aux.TheMatrix[fila, columna] != '0') aux.TheMatrix[fila, columna] = '0';
                        }
                        else aux.TheMatrix[fila, columna] = '0';
                    }
                }
            }

            main.clippingMatrix(aux.TheMatrix);

            pintarm.printMatrix(main.TheMatrix);
        }

        private void CaiciondeLetras(int cantidad, Random rnd)
        {
            for (int i = 0; i < cantidad; i++)
            {
                charColumna = rnd.Next(0, main.TheMatrix.GetLength(1));
                letra = rnd.Next(0, letters.Length - 1);
                main.TheMatrix[0, charColumna] = letters[letra];
            }
        }



    }
}
