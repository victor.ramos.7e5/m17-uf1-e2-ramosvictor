﻿using MyFirstProgram;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using matrixFinal;

namespace M17_UF1_E2_Ramos_Victor
{
    public class PrintMatrix2 : MatrixRepresentation

    {
        public void printMatrix(char[,] _theMatrix)
        {
            Console.Clear();
            for (int x = 0; x < _theMatrix.GetLength(0); x++)
            {
                for (int y = 0; y < _theMatrix.GetLength(1); y++)
                {
                    Console.ResetColor();
                    if (_theMatrix[x, y] != '0')
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write($" {_theMatrix[x, y]}");
                    }
                    else
                    {
                        Console.ForegroundColor = ConsoleColor.Black;
                        Console.Write($" {_theMatrix[x, y]}");
                    }
                }

                Console.WriteLine();
            }
        }

        public override char[,] clippingMatrix(char[,] toClipper)
        {
            int limitX = (toClipper.GetLength(0) > _theMatrix.GetLength(0))
                ? _theMatrix.GetLength(0)
                : toClipper.GetLength(0);
            int limitY = (toClipper.GetLength(1) > _theMatrix.GetLength(1))
                ? _theMatrix.GetLength(1)
                : toClipper.GetLength(1);

            for (int x = 0; x < limitX; x++)
            {
                for (int y = 0; y < limitY; y++)
                {
                    _theMatrix[x, y] = toClipper[x, y];
                }
            }

            return _theMatrix;
        }

    }
}